This sample app illustrates how to upload content to a Web service using OkHttp,
where the content is identified by a `Uri`, and you need to use a multipart form
upload. It is based on [this implementation](https://github.com/square/okhttp/issues/3585#issuecomment-327319196).
Full credit goes to Jared Burrows and Jake Wharton, who collaborated
on that issue to work out an implementation of a `RequestBody` that
can handle an Android `Uri`.

To build this project, you will need to add a definition of
`DEMO_REQUEST_BIN` to the project's `gradle.properties` or some similar
location (e.g., `~/.gradle/gradle.properties` on Linux and macOS).
This needs to point to the URL that you want the sample to `POST` to.
This app was tested using [RequestBin](https://requestbin.com/).
