/*
  Copyright (c) 2019 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.
*/

package com.commonsware.multipartupload

import android.app.Application
import android.net.Uri
import android.util.Log
import androidx.documentfile.provider.DocumentFile
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.BroadcastChannel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.MultipartBody
import okhttp3.OkHttpClient
import okhttp3.Request
import java.io.IOException

private const val TAG = "MultipartUpload"
private const val DEFAULT_TYPE = "application/octet-stream"

class MainMotor(app: Application) : AndroidViewModel(app) {
  private val ok by lazy { OkHttpClient() }
  private val _events = BroadcastChannel<String>(Channel.BUFFERED)
  val events = _events.asFlow()

  fun upload(content: Uri) {
    viewModelScope.launch {
      try {
        uploadMultipart(content)
      } catch (t: Throwable) {
        Log.e(TAG, "Exception doing upload", t)
        _events.send("BOOOOOOM!!!")
      }
    }
  }

  private suspend fun uploadMultipart(content: Uri) =
    withContext(Dispatchers.IO) {
      val resolver = getApplication<Application>().contentResolver
      val doc = DocumentFile.fromSingleUri(getApplication(), content)!!
      val type = (doc.type ?: DEFAULT_TYPE).toMediaType()
      val contentPart = InputStreamRequestBody(type, resolver, content)

      val requestBody = MultipartBody.Builder()
        .setType(MultipartBody.FORM)
        .addFormDataPart("author", "CommonsWare")
        .addFormDataPart("something", doc.name, contentPart)
        .build()

      val request = Request.Builder()
        .url(BuildConfig.SERVER_URL)
        .post(requestBody)
        .build()

      ok.newCall(request).execute().use { response ->
        if (!response.isSuccessful) throw IOException("Unexpected code $response")

        _events.send(response.body!!.string())
      }
    }
}