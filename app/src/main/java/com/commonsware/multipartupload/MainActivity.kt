/*
  Copyright (c) 2019 CommonsWare, LLC

  Licensed under the Apache License, Version 2.0 (the "License"); you may not
  use this file except in compliance with the License. You may obtain	a copy
  of the License at http://www.apache.org/licenses/LICENSE-2.0. Unless required
  by applicable law or agreed to in writing, software distributed under the
  License is distributed on an "AS IS" BASIS,	WITHOUT	WARRANTIES OR CONDITIONS
  OF ANY KIND, either express or implied. See the License for the specific
  language governing permissions and limitations under the License.
*/

package com.commonsware.multipartupload

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.commonsware.multipartupload.databinding.ActivityMainBinding
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

private const val REQUEST_UPLOAD = 1337

class MainActivity : AppCompatActivity() {
  private val motor: MainMotor by viewModels()

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    val binding = ActivityMainBinding.inflate(layoutInflater)

    setContentView(binding.root)

    binding.upload.setOnClickListener {
      startActivityForResult(
        Intent(Intent.ACTION_OPEN_DOCUMENT)
          .addCategory(Intent.CATEGORY_OPENABLE)
          .setType("*/*"),
        REQUEST_UPLOAD
      )
    }

    lifecycleScope.launch {
      motor.events.collect {
        Toast.makeText(this@MainActivity, it, Toast.LENGTH_LONG).show()
      }
    }
  }

  override fun onActivityResult(
    requestCode: Int,
    resultCode: Int,
    data: Intent?
  ) {
    if (requestCode == REQUEST_UPLOAD) {
      if (resultCode == Activity.RESULT_OK) {
        data?.data?.let { motor.upload(it) }
      }
    } else {
      super.onActivityResult(requestCode, resultCode, data)
    }
  }
}